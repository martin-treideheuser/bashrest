#!/usr/bin/env bash

rm -f out
mkfifo out
trap "rm -f out" EXIT

function setup_environment() {
    if [ -e "/etc/os-release" ]; then
        echo " "
        cat /etc/os-release | grep -e "^NAME="
        echo " "
        NAME=$(cat /etc/os-release | grep -e "^NAME=" | awk -F "=" {'print tolower($2)'})
        case "${NAME}" in
            *alpine*) NCOPTS="nc -l -p 8888";;
            *ubuntu*) NCOPTS="nc -l -p 8888";;
            *centos*) NCOPTS="ncat -l 8888";;
            *) NCOPTS="nc -c -l -p 8888";;
        esac
    elif [ -e "/etc/centos-release" ]; then
        NCOPTS="ncat -l 8888"
    else
        NCOPTS="nc -c -l -p 8888"
    fi
}

function date_check() {
    date
}

function ping_check() {
    ping -c1 -W1 ${1} 2>&1> /dev/null && echo "Ping of ${1} ok" || echo "Ping of ${1} error"
}

setup_environment
echo "Simple Bash http REST endpoint started..."
while true
do
  cat out | ${NCOPTS} > >( # parse the netcat output, to build the answer redirected to the pipe "out".
    export REQUEST=
    while read line
    do
      sleep 0.2
      line=$(echo "${line}" | tr -d '[\r\n]')

      if echo "${line}" | grep -qE '^GET /' # if line starts with "GET /"
      then
        REQUEST=$(echo "${line}" | cut -d ' ' -f2) # extract the request
      elif [ "x${line}" = x ] # empty line / end of request
      then
        HTTP_200="HTTP/1.1 200 OK"
        HTTP_LOCATION="Location:"
        HTTP_404="HTTP/1.1 404 Not Found"
        # call a script here
        # Note: REQUEST is exported, so the script can parse it (to answer 200/403/404 status code + content)
        if echo ${REQUEST} | grep -qE '^/echo/'
        then
            printf "%s\n%s %s\n\n%s\n" "$HTTP_200" "$HTTP_LOCATION" $REQUEST ${REQUEST#"/echo/"} > out
        elif echo ${REQUEST} | grep -qE '^/date_check'
        then
            printf "%s\n%s %s\n\n%s\n" "$HTTP_200" "$HTTP_LOCATION" "$REQUEST" "$(date_check)" > out
        elif echo ${REQUEST} | grep -qE '^/ping_check'
        then
            printf "%s\n%s %s\n\n%s\n" "$HTTP_200" "$HTTP_LOCATION" "$REQUEST" "$(ping_check ${REQUEST#"/ping_check/"})" > out
        else
            printf "%s\n%s %s\n\n%s\n" "$HTTP_404" "$HTTP_LOCATION" $REQUEST "Resource $REQUEST NOT FOUND!" > out
        fi
      fi
    done
  )
done
